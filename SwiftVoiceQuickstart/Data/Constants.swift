//
//  Constants.swift
//  SwiftVoiceQuickstart
//
//  Created by JellyNoon on 22/1/2563 BE.
//  Copyright © 2563 Twilio, Inc. All rights reserved.
//

import Foundation

let HOST = "http://mocky.meevuu.com/poc_ladyclinic"
let JSON_HEADER  = ["Content-Type" : "application/json"]

enum APIError: Error {
    case unauthorize
    case somethingWentWrong
    case onNetworkNotAvailable
    case invalidData
    case networkError
    case rawString(message:String)
    
    var localizedDescription: String {
        switch self {
        case .unauthorize: return "UNAUTHORIZE"
        case .invalidData: return "INVALID_DATA"
        case .networkError: return "NETWORK_FAILED"
        case .onNetworkNotAvailable: return "NETWORK_NOT_AVAILABLE"
        case .somethingWentWrong:return "SOMETHING_WENT_WRONG"
        case .rawString(let message):return message
        }
    }
}
