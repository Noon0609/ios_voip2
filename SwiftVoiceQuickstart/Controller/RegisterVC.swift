//
//  RegisterVC.swift
//  SwiftVoiceQuickstart
//
//  Created by JellyNoon on 21/1/2563 BE.
//  Copyright © 2563 Twilio, Inc. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {
    
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    weak var owner: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveBtn.layer.cornerRadius = 10
        saveBtn.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        print("register viewWillDisappear identity \(identity)")
    }
    
    @IBAction func savePressed(_ sender: Any) {
        
        if let name = nameTextField.text {
            identity = name
            switch owner {
            case is ViewController:
                self.dismiss(animated: true, completion: nil)
            default:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "menuListId")
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
}
