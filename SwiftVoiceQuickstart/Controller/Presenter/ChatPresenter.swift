//
//  ChatPresenter.swift
//  SwiftVoiceQuickstart
//
//  Created by JellyNoon on 22/1/2563 BE.
//  Copyright © 2563 Twilio, Inc. All rights reserved.
//

import Foundation

class ChatPresenter: ChatInterface, ChatInteractorOutput {
    var input: ChatInteractorInput {return self}
    var output: ChatInteractorOutput {return self}
    
    var getMessageSuccess: StrCB?
    var getMessageError: StrCB?
    var sendMessageSuccess: StrCB?
    var sendMessageError: StrCB?
    var message: [Message]?
    var messageList: [NoonMessage]!
    var user: MockUser?
    
    init() {
        messageList = []
        user = MockUser(senderId: "noon", displayName: "Afternoon")
    }
}

extension ChatPresenter: ChatInteractorInput {
    func getAllMessage(groupId: String) {
        FirebaseService.instance.getAllMessage(groupId: groupId) { (response: [Message]?, erroßr: APIError?) in
            guard let responseData = response
                else {
                    self.getMessageError?("error")
                    return
            }
            self.message = responseData
            self.messageList.removeAll()
            for item in responseData {
                guard let text = item.text
                ,let id = item.id else { return }
                let someMem = Member(name: id, color: .random)
                let message = NoonMessage(member: someMem, text: text, messageId: UUID().uuidString)
                self.messageList.append(message)
            }
            self.getMessageSuccess?("success")
        }
    }
    
    func sendMessage(groupId: String, id: String, message: String) {
        FirebaseService.instance.sendMessage(id: id, groupId: groupId, message: message) { (isSuccess) in
            if isSuccess {
                self.sendMessageSuccess?("success")
            } else {
                self.sendMessageError?("error")
            }
        }
    }
}
