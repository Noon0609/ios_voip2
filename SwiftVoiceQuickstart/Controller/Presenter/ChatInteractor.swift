//
//  ChatInteractor.swift
//  SwiftVoiceQuickstart
//
//  Created by JellyNoon on 22/1/2563 BE.
//  Copyright © 2563 Twilio, Inc. All rights reserved.
//

import Foundation

protocol ChatInteractorInput {
    func getAllMessage(groupId: String)
    func sendMessage(groupId: String, id: String, message: String)
}

protocol ChatInteractorOutput: class {
    var getMessageSuccess: StrCB? { get set}
    var getMessageError: StrCB? {get set}
    var sendMessageSuccess: StrCB? { get set}
    var sendMessageError: StrCB? { get set}
    var message: [Message]? { get set}
    var messageList: [NoonMessage]! { get set}
    var user: MockUser? { get set}
}

protocol ChatInterface: ChatInteractorInput, ChatInteractorOutput {
    var input: ChatInteractorInput { get}
    var output: ChatInteractorOutput { get}
}
