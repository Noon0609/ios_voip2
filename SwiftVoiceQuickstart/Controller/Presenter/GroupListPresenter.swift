//
//  GroupListPresenter.swift
//  SwiftVoiceQuickstart
//
//  Created by JellyNoon on 22/1/2563 BE.
//  Copyright © 2563 Twilio, Inc. All rights reserved.
//

import Foundation

class GroupListPresenter: GroupListInterface, GroupListInteractorOutput {
    var input: GroupListInteractorInput {return self}
    var output: GroupListInteractorOutput {return self}
    var getGroupListSuccess: StrCB?
    var getGroupListError: StrCB?
    var groupList: [Group]?
    
}

extension GroupListPresenter: GroupListInteractorInput {
    func getGroupList() {
        let router = Router.getGroupList()
        Service.instance.request(router: router) { (response: GroupListResponse?, error: APIError?) in
            guard let responseData = response
                ,let code = response?.code
                ,let group = responseData.responseObject?.groups
                else {
                    self.getGroupListError?("error")
                    return
            }
            self.groupList = group
            self.getGroupListSuccess?("success")
        }
    }
}
