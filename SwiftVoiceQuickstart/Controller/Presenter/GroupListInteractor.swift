//
//  GroupListInteractor.swift
//  SwiftVoiceQuickstart
//
//  Created by JellyNoon on 22/1/2563 BE.
//  Copyright © 2563 Twilio, Inc. All rights reserved.
//

import Foundation
typealias StrCB = ((String) -> ())
protocol GroupListInteractorInput {
    func getGroupList()
}

protocol GroupListInteractorOutput: class {
    var getGroupListSuccess: StrCB? { get set}
    var getGroupListError: StrCB? { get set}
    var groupList: [Group]? { get set}
}

protocol GroupListInterface: GroupListInteractorInput, GroupListInteractorOutput {
    var input: GroupListInteractorInput { get}
    var output: GroupListInteractorOutput { get}
}
