//
//  GroupListVC.swift
//  SwiftVoiceQuickstart
//
//  Created by JellyNoon on 22/1/2563 BE.
//  Copyright © 2563 Twilio, Inc. All rights reserved.
//

import UIKit

class GroupListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var groupCellId = "groupCellId"
    var presenter: GroupListInterface!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure(GroupListPresenter())
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "GroupTableViewCell", bundle: nil), forCellReuseIdentifier: groupCellId)
//        FirebaseService.instance.getAllMessage(groupId: "003") { (response: [Message]?, error: APIError?) in
        
//        }
        presenter.input.getGroupList()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let group = presenter.output.groupList
        return group?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: groupCellId, for: indexPath) as! GroupTableViewCell
        guard let group = presenter.output.groupList else { return cell}
        cell.titleText.text = group[indexPath.row].title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let group = presenter.output.groupList else { return }
//        let vc = ChatVC(nibName: "ChatVC", bundle: nil)
        let vc = MessageVC()
        vc.id = group[indexPath.row].id
        vc.user = group[indexPath.row].users
        vc.chatRoom = group[indexPath.row].title
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func configure(_ interface: GroupListInterface) {
        self.presenter = interface
        bindPresenter()
    }
    
    func bindPresenter() {
        presenter.output.getGroupListSuccess = getGroupListSuccess()
        presenter.output.getGroupListError = getGroupListError()
    }
    
    func getGroupListSuccess() -> ((String) -> ()) {
        return { [weak self] message in
            self?.tableView.reloadData()
        }
    }
    
    func getGroupListError() -> ((String) -> ()) {
        return { [weak self] message in
        }
    }

}

