//
//  MenuListVC.swift
//  SwiftVoiceQuickstart
//
//  Created by JellyNoon on 22/1/2563 BE.
//  Copyright © 2563 Twilio, Inc. All rights reserved.
//

import UIKit

class MenuListVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func callPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "callerId")
        //        controller.modalPresentationStyle = .overFullScreen
        //        controller.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func chatPressed(_ sender: Any) {
        let vc = GroupListVC(nibName: "GroupListVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
