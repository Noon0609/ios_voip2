//
//  ViewController.swift
//  ScaledroneChatTest
//
//  Created by Marin Benčević on 08/09/2018.
//  Copyright © 2018 Scaledrone. All rights reserved.
//

import UIKit
import MessageKit

struct NoonMessage:MessageType {
    let member: Member
    let text: String
    let messageId: String
}

struct Member {
    let name: String
    let color: UIColor
}

extension NoonMessage {
    var sender: SenderType {
        return Sender(id: member.name, displayName: member.name)
    }
    
    var sentDate: Date {
        return Date()
    }
    
    var kind: MessageKind {
        return .text(text)
    }
}

class MessageVC: MessagesViewController {
    
    var messages: [NoonMessage] = []
    var member: Member!
    var counter = 0
    var id: String?
    var user: [User]?
    var timer = Timer()
    var presenter: ChatInterface!
    var otherUser: User?
    var iAm: User?
    var chatRoom: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure(ChatPresenter())
        
        guard let groupId = id else { return}
        presenter.input.getAllMessage(groupId: groupId)
        
        member = Member(name: "noon", color: .random)
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messageInputBar.delegate = self
        messagesCollectionView.messagesDisplayDelegate = self

        //        addMessage()
//        if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
//            layout.textMessageSizeCalculator.outgoingAvatarSize = .zero
//            layout.textMessageSizeCalculator.incomingAvatarSize = .zero
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.topItem?.title = chatRoom
    }
    
//    func setupTimer(){
//        timer.invalidate() // just in case this button is tapped multiple times
//
//        // start the timer
//        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
//    }
    
    
//    @objc func timerAction() {
//        counter += 1
//        addMessage()
//        sleep(1)
//        myMessage(str: "force")
//    }
//
//    func addMessage(){
//        let someMem = Member(name: .randomName, color: .random)
//
//        let number = Int.random(in: 15 ..< 250)
//        let str = randomString(length: number)
//
//        let message = NoonMessage(member: someMem, text:str, messageId: UUID().uuidString)
//        self.messages.append(message)
//        self.messagesCollectionView.reloadData()
//        self.messagesCollectionView.scrollToBottom(animated: true)
//
//    }
//
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
//
//    func randomInt(digits:Int) -> Int {
//        let min = Int(pow(Double(10), Double(digits-1))) - 1
//        let max = Int(pow(Double(10), Double(digits))) - 1
//        return Int(Range(uncheckedBounds: (min, max)))
//    }
}

extension MessageVC: MessagesDataSource {
    func currentSender() -> SenderType {
        return Sender(id: member.name, displayName: member.name)
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return presenter.output.messageList.count
    }
    
    func currentSender() -> Sender {
        return Sender(id: member.name, displayName: member.name)
    }
    
    func messageForItem(at indexPath: IndexPath,
                        in messagesCollectionView: MessagesCollectionView) -> MessageType {
        
        return presenter.output.messageList[indexPath.section]
    }
    
    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 14
    }
    
    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let senderId = presenter.output.messageList[indexPath.section]
        let member = user?.first(where: {$0.id == senderId.member.name})
        let name = member?.name ?? "Unknow"
        return NSAttributedString(
            string: name,
            attributes: [.font: UIFont.systemFont(ofSize: 12)])
    }
    
    func configure(_ interface: ChatInterface) {
        self.presenter = interface
        bindPresenter()
    }
    
    //MARK: bind Presenter
    func bindPresenter() {
        presenter.output.getMessageSuccess = getMessageSuccess()
        presenter.output.getMessageError = getMessageError()
        presenter.output.sendMessageSuccess = sendMessageSuccess()
        presenter.output.sendMessageError = sendMessageError()
    }
    
    func getMessageSuccess() -> ((String) -> ()) {
        return { [weak self] message in
            self?.messagesCollectionView.reloadData()
            self?.messagesCollectionView.scrollToBottom(animated: true)
            print("count \(self?.presenter.output.messageList.count)")
        }
    }
    
    func getMessageError() -> ((String) -> ()) {
        return { [weak self] message in
        }
    }
    
    func sendMessageSuccess() -> ((String) -> ()) {
        return { [weak self] message in
            
        }
    }
    
    func sendMessageError() -> ((String) -> ()) {
        return { [weak self] message in
        }
    }
}

extension MessageVC: MessagesLayoutDelegate {
    func heightForLocation(message: MessageType,
                           at indexPath: IndexPath,
                           with maxWidth: CGFloat,
                           in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 0
    }
}

extension MessageVC: MessagesDisplayDelegate {
    func configureAvatarView(
        _ avatarView: AvatarView,
        for message: MessageType,
        at indexPath: IndexPath,
        in messagesCollectionView: MessagesCollectionView) {
        
        let senderId = presenter.output.messageList[indexPath.section]
        let member = user?.first(where: {$0.id == senderId.member.name})
        let image = member?.imageUrl
        avatarView.loadImage(link: image)
    }
}

extension MessageVC: MessageInputBarDelegate {
    
    func inputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        guard let groupId = id else { return}
        presenter.input.sendMessage(groupId: groupId, id: "noon", message: text)
        inputBar.inputTextView.text = ""
    }
    
    
    func myMessage(str:String){
        let number = Int.random(in: 15 ..< 150)
        let myStr = randomString(length: number)
        let message = NoonMessage(member: member, text: myStr, messageId: UUID().uuidString)
        self.messages.append(message)
        self.messagesCollectionView.reloadData()
        self.messagesCollectionView.scrollToBottom(animated: true)
    }
    
}

extension Int {
    init(_ range: Range<Int> ) {
        let delta = range.startIndex < 0 ? abs(range.startIndex) : 0
        let min = UInt32(range.startIndex + delta)
        let max = UInt32(range.endIndex   + delta)
        self.init(Int(min + arc4random_uniform(max - min)) - delta)
    }
}

