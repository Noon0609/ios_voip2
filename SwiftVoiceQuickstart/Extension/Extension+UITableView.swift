//
//  Extension+UITableView.swift
//  SwiftVoiceQuickstart
//
//  Created by JellyNoon on 22/1/2563 BE.
//  Copyright © 2563 Twilio, Inc. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func scrollToLastCell(animated : Bool) {
        let lastSectionIndex = self.numberOfSections - 1 // last section
        let lastRowIndex = self.numberOfRows(inSection: lastSectionIndex) - 1 // last row
        self.scrollToRow(at: IndexPath(row: lastRowIndex, section: lastSectionIndex), at: .bottom, animated: animated)
    }
}
