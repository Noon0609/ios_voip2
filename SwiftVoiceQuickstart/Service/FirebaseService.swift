//
//  FirebaseService.swift
//  SwiftVoiceQuickstart
//
//  Created by JellyNoon on 22/1/2563 BE.
//  Copyright © 2563 Twilio, Inc. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

let DB_BASE = Database.database().reference()

class FirebaseService {
    static var instance = FirebaseService()
    
//    private var _REF_BASE = DB_BASE
//    private var _REF_USERS = DB_BASE.child("users_noon")
//    private var _REF_GROUPS = DB_BASE.child("002")
//    private var _REF_FEED = DB_BASE.child("feed")
//    
//    var REF_BASE: DatabaseReference {
//        return _REF_BASE
//    }
//    
//    var REF_USERS: DatabaseReference {
//        return _REF_USERS
//    }
//    
//    var REF_GROUPS: DatabaseReference {
//        return _REF_GROUPS
//    }
//    
//    var REF_FEED: DatabaseReference {
//        return _REF_FEED
//    }
    
    func getAllMessage(groupId: String, completion: @escaping (_ response: [Message]?,_ error: APIError?) -> Void) {
        let group = DB_BASE.child(groupId)
        group.child("messages").observe(.value, with: { (groupMessageSnapshot) in
            guard let groupMessageSnapshot = groupMessageSnapshot.children.allObjects as? [DataSnapshot] else { return }
//            print("groupMessageSnapshot \(groupMessageSnapshot)")
            var messages = [Message]()
            for groupMessage in groupMessageSnapshot {
                let id = groupMessage.childSnapshot(forPath: "id").value as! String
                let text = groupMessage.childSnapshot(forPath: "text").value as! String
                let key = groupMessage.key
                
                let message = Message(id: id, text: text, key: key)
                messages.append(message)
            }
            
            completion(messages, nil)
        })
    }
    
    func sendMessage(id: String, groupId: String, message: String, sendComplete: @escaping (_ status: Bool) -> ()) {
        let message2 = Message(id: id, text: message, key: "")
        let group = DB_BASE.child(groupId)
        let object = message2.toJSON()
        group.child("messages").childByAutoId().setValue(object)
       sendComplete(true)
    }
}
