//
//  Service.swift
//  WiseRoom
//
//  Created by Noon nps on 26/12/2562 BE.
//  Copyright © 2562 Wisdom Vast. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class Service {
    static let instance = Service()
    
    private let timeout:Double = 25
    var alamoFireManager : SessionManager?
    
    private init () {
        setupTimeout()
    }
    
    private func setupTimeout(){
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = timeout
        configuration.timeoutIntervalForResource = timeout
        alamoFireManager = Alamofire.SessionManager(configuration: configuration) // not in this line
    }
    
    
    func request<T: Mappable>(router: Router?, onResponse: @escaping (_ data: T?,_ error: APIError?) -> Void) {
        
        guard let url = router
            else{
                return onResponse(nil, .invalidData)
        }
        alamoFireManager?.request(url)
//            .validate(statusCode: 200..<401)
        .debugLog()
            .responseObject { (response: DataResponse<T>) in
                
                guard let data = response.data else {
                    onResponse(nil, .networkError)
                    return
                    
                }
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                                    print("Data: \(utf8Text)")
                }
                
                switch response.result {
                case .success(let value):
                    onResponse(value as T, nil)
                    break
                case .failure(let error):
                    let err: NSError = error as NSError
                    switch err.code {
                    case 401:
                        onResponse(nil, .unauthorize)
                    case 400 ... 499:
                        onResponse(nil, .rawString(message: "Error \(err.code)"))
                        break
                    case 500 ... 599:
                        onResponse(nil, .rawString(message: "Server error \(err.code)"))
                        break
                    case -1001:
                        onResponse(nil, .networkError)
                        break
                    case -1009:
                        onResponse(nil, .onNetworkNotAvailable)
                        break
                    default:
//                        print("HTTP CODE \(response.response?.statusCode)")
                        let val = response.result.value as? BaseResponse
                        if let bodyMessage = response.result.value as? BaseResponse,
                            let message = bodyMessage.message {
                            onResponse(nil, .rawString(message: message))
                            break
                        }
                        onResponse(nil, .rawString(message: "Something went wrong"))
                        break
                    }
                }
        }
    }
}

extension Request {
    public func debugLog() -> Self {
        #if DEBUG
        debugPrint(self)
        #endif
        return self
    }
}
