//
//  Router.swift
//  WiseRoom
//
//  Created by Noon nps on 26/12/2562 BE.
//  Copyright © 2562 Wisdom Vast. All rights reserved.
//

import Foundation
import Alamofire

public enum Router: URLRequestConvertible {
    
    public func asURLRequest() throws -> URLRequest {
        switch self {
        default:
            
            let baseURLString = HOST
            let url = URL(string: baseURLString + path)!
            var mutableURLRequest = URLRequest(url: url)
            mutableURLRequest.httpMethod = method.rawValue
            
            setHttpHeaders(&mutableURLRequest, headers: headers)
            do{
                let encode = try URLEncoding.queryString.encode(mutableURLRequest, with: parameters)
                return encode
            }
            catch{
                return URLRequest(url: URL(string: "")!)
            }
        }
    }
    case getGroupList()
}


