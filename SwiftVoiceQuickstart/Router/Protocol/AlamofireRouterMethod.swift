//
//  AlamofireRouterMethod.swift
//  WiseRoom
//
//  Created by Noon nps on 26/12/2562 BE.
//  Copyright © 2562 Wisdom Vast. All rights reserved.
//

import Foundation
import Alamofire

extension Router {
    public var method: Alamofire.HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
}
