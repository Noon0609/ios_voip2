//
//  AlamofireRouterPath.swift
//  WiseRoom
//
//  Created by Noon nps on 26/12/2562 BE.
//  Copyright © 2562 Wisdom Vast. All rights reserved.
//

import Foundation

extension Router {
    public var path: String {
        switch self {
        case .getGroupList():
            return "/grouplist"
        }
    }
}
