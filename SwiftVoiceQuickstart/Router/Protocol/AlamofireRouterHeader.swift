//
//  AlamofireRouterHeader.swift
//  WiseRoom
//
//  Created by Noon nps on 26/12/2562 BE.
//  Copyright © 2562 Wisdom Vast. All rights reserved.
//

import Foundation

extension Router {
    
    public var headers: [String: String]? {
        switch self {
        
        default:
            return ["X-Auth-Token": "UserManager.instance.getToken()"]
        }
    }
    
    public func setHttpHeaders(_ mutableURLRequest: inout URLRequest, headers: [String: String]?) {
        
        addDefaultHttpHeader(&mutableURLRequest)
        if let headers = headers {
            for each in headers.keys {
                mutableURLRequest.setValue(headers[each], forHTTPHeaderField: each)
            }
        }
    }
    
    private func addDefaultHttpHeader(_ mutableURLRequest: inout URLRequest) {
//        guard let token = UserManager.instance.getToken() else { return }
        mutableURLRequest.setValue("application/json;charset=utf-8", forHTTPHeaderField: "Content-Type")

    }
}
