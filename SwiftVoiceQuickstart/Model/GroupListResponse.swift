//
//  GroupListResponse.swift
//  SwiftVoiceQuickstart
//
//  Created by JellyNoon on 22/1/2563 BE.
//  Copyright © 2563 Twilio, Inc. All rights reserved.
//

import Foundation
import ObjectMapper

class GroupListResponse: BaseResponse {
    var responseObject: GroupListResponseData?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        responseObject <- map["responseObject"]
    }
}

class GroupListResponseData: Mappable {
    var groups: [Group]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        groups <- map["groups"]
    }
}

struct Group: Mappable {
    var id: String?
    var title: String?
    var users: [User]?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        users <- map["users"]
    }
}

struct User: Mappable {
    var id: String?
    var imageUrl: String?
    var name: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        imageUrl <- map["imageUrl"]
        name <- map["name"]
    }
    
    
}
