//
//  MessageResponse.swift
//  SwiftVoiceQuickstart
//
//  Created by JellyNoon on 22/1/2563 BE.
//  Copyright © 2563 Twilio, Inc. All rights reserved.
//

import Foundation
import ObjectMapper


struct Message: Mappable {
    var id: String?
    var text: String?
    var key : String?
    
    init(id: String, text: String, key: String) {
        self.id = id
        self.text = text
        self.key = key
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        text <- map["text"]
        key <- map["key"]
    }
}
